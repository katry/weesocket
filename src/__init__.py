try:
	from .client import Client
	from .server import Server
except Exception:
	pass

__version__ = "__VER__"

# weesocket changelog

## v2.0.0
- major refactoring 
- functionality update
- pytest improvements
- readme updated
- CI automations

## v1.0.1 - v1.0.3
- minor bugfixes

## v1.0.0
- initial release

from os import remove
from time import sleep
from pytest import mark
from .conftest import (
	ClientWrapper, ServerWrapper,
	init_connections, try_until_true,
	public_key_server, private_key_server,
	public_key_client, private_key_client
)

client_configs = {
	"file_socket": {"socket_file": "/tmp/weesocket-test.sock"},
	"host_port": {"host": "0.0.0.0", "port": 55555},
	"secret": {"secret": b"client-secret", "remote_secret": b"server-secret"},
	"encrypt": {"encrypt": True},
	"not_encrypt": {"encrypt": False},
}
server_configs = {
	"file_socket": {"socket_file": "/tmp/weesocket-test.sock"},
	"host_port": {"host": "0.0.0.0", "port": 55555},
	"secret": {"secret": b"server-secret", "remotes": [b"client-secret"]},
	"encrypt": {"encrypt": True},
	"not_encrypt": {"encrypt": False}
}

parametrization = [
	(
		{**server_configs["file_socket"], **server_configs["not_encrypt"]},
		{**client_configs["file_socket"], **client_configs["not_encrypt"]},
	),
	(
		{**server_configs["host_port"], **server_configs["not_encrypt"]},
		{**client_configs["host_port"], **client_configs["not_encrypt"]},
	),

	(
		{**server_configs["file_socket"], **server_configs["encrypt"]},
		{**client_configs["file_socket"], **client_configs["encrypt"]},
	),
	(
		{**server_configs["host_port"], **server_configs["encrypt"]},
		{**client_configs["host_port"], **client_configs["encrypt"]},
	),

	(
		{**server_configs["file_socket"], **server_configs["not_encrypt"], **server_configs["secret"]},
		{**client_configs["file_socket"], **client_configs["not_encrypt"], **client_configs["secret"]},
	),
	(
		{**server_configs["host_port"], **server_configs["not_encrypt"], **server_configs["secret"]},
		{**client_configs["host_port"], **client_configs["not_encrypt"], **client_configs["secret"]},
	),

	(
		{**server_configs["file_socket"], **server_configs["encrypt"], **server_configs["secret"]},
		{**client_configs["file_socket"], **client_configs["encrypt"], **client_configs["secret"]},
	),
	(
		{**server_configs["host_port"], **server_configs["encrypt"], **server_configs["secret"]},
		{**client_configs["host_port"], **client_configs["encrypt"], **client_configs["secret"]},
	),
]


@mark.parametrize("server_params,client_params", parametrization)
def test_init(server_params, client_params):
	try:
		remove("/tmp/weesocket-test-%s.sock" % sock_id)
	except Exception:
		pass
	server_failed = False
	try:
		s = ServerWrapper(
			id="test-server", **server_params,
			# public_key=public_key_server, private_key=private_key_server
		)
	except Exception:
		server_failed = True

	c_id = None
	counter = 0
	while not c_id:
		counter += 1
		if counter == 30:
			break
		client_failed = False
		try:
			c = ClientWrapper(
				**client_params,
				# public_key=public_key_client, private_key=private_key_client
			)
		except Exception:
			client_failed = True
		sleep(1)
		c_id = c.id

	assert server_failed == False
	assert client_failed == False
	sleep(5)
	try_until_true(lambda: c.id is not None)
	assert s.id == "test-server"
	assert c.id and c.id.startswith("client_")
	assert "client_1" in s._connections
	s.stop()
	double_stop_error = False
	try:
		s.stop()
	except Exception:
		double_stop_error = True
	assert double_stop_error == False


@mark.parametrize("server_params,client_params", parametrization)
def test_send(server_params, client_params):
	server, clients = init_connections(server_params, client_params, count_of_clients=1)
	server_send_dict = {"test": "server-send", "int": 1, "bool": True}
	client_send_dict = {"test": "client-send", "int": .1, "bool": False}
	large_dict = {"dictkey_%s" % i : "value_%s" % i for i in range(1000)}
	server_send_dict_large = {"test": "server-send", **large_dict}
	client_send_dict_large = {"test": "client-send", **large_dict}

	server.send(server_send_dict, target=clients[0].id)
	try_until_true(lambda: hasattr(clients[0], "last"))
	assert clients[0].last["payload"] == server_send_dict
	assert clients[0].last["sender"] == "test-server"

	clients[0].send(client_send_dict, target="test-server")
	try_until_true(lambda: hasattr(server, "last"))
	assert server.last["payload"] == client_send_dict
	assert server.last["sender"] == clients[0].id

	server.send(server_send_dict_large, target=clients[0].id)
	try_until_true(lambda: clients[0].last["payload"] != server_send_dict)
	assert clients[0].last["payload"] == server_send_dict_large
	assert clients[0].last["sender"] == "test-server"

	clients[0].send(client_send_dict_large, target="test-server")
	try_until_true(lambda: server.last["payload"] != client_send_dict)
	assert server.last["payload"] == client_send_dict_large
	assert server.last["sender"] == clients[0].id
	server.stop()


@mark.parametrize("server_params,client_params", [
	(
		{"host": "0.0.0.0", "port": 55556, "encrypt": False},
		{"host": "0.0.0.0", "port": 55556, "encrypt": False}
	)
])
def test_queued_send(server_params, client_params):
	try:
		remove("/tmp/weesocket-test.sock")
	except Exception:
		pass

	counter = 0
	while counter < 10:
		s = ServerWrapper(
			id="test-server",
			**server_params,
			public_key=public_key_server, private_key=private_key_server
		)
		c = ClientWrapper(
			**client_params,
			public_key=public_key_client, private_key=private_key_client
		)
		message = {"test-message": "test"}
		c.send(message)
		assert (message, "") in c._queue
		try_until_true(lambda: c.id is not None, attempts=30)
		if c.id:
			break
		s.stop()
		counter += 1

	try_until_true(lambda: hasattr(s, "last"))

	assert s.last["payload"] == message
	assert s.last["sender"] == c.id
	s.stop()


@mark.parametrize("server_params,client_params", parametrization)
def test_proxy(server_params, client_params):
	server, clients = init_connections(server_params, client_params, count_of_clients=3)
	message_1 = {"test": "proxy", "int": 1, "bool": False}
	message_2 = {"test": "proxy2", "int": 2, "bool": True}
	message_3 = {"test": "proxy", "int": 3, "bool": False}

	clients[0].send(message_1, target=clients[1].id)
	try_until_true(lambda: hasattr(clients[1], "last"))
	assert hasattr(server, "last") == False
	assert hasattr(clients[0], "last") == False

	assert clients[1].last["payload"] == message_1
	assert clients[1].last["sender"] == clients[0].id

	assert hasattr(clients[2], "last") == False

	clients[2].send(message_2, target=clients[1].id)
	try_until_true(lambda: clients[1].last["sender"] != clients[0].id)
	assert hasattr(server, "last") == False
	assert hasattr(clients[0], "last") == False

	assert clients[1].last["payload"] == message_2
	assert clients[1].last["sender"] == clients[2].id

	assert hasattr(clients[2], "last") == False

	clients[2].send(message_3, target=clients[0].id)
	try_until_true(lambda: hasattr(clients[0], "last"))
	assert hasattr(server, "last") == False
	assert clients[0].last["payload"] == message_3
	assert clients[0].last["sender"] == clients[2].id

	assert clients[1].last["payload"] == message_2
	assert clients[1].last["sender"] == clients[2].id

	assert hasattr(clients[2], "last") == False
	server.stop()


@mark.parametrize("server_params,client_params", parametrization)
def test_broadcast(server_params, client_params):
	server, clients = init_connections(server_params, client_params, count_of_clients=3)
	br = {"test": "br", "int": 1, "bool": False}
	br_with_server = {"test": "brws", "int": 2, "bool": True}
	br_from_server = {"test": "brfs", "int": 3, "bool": False}

	clients[0].send(br, target="*")
	sleep(2)
	assert hasattr(server, "last") == False
	assert hasattr(clients[0], "last") == False

	assert clients[1].last["payload"] == br
	assert clients[1].last["sender"] == clients[0].id

	assert clients[2].last["payload"] == br
	assert clients[2].last["sender"] == clients[0].id

	server.send(br_from_server)
	sleep(2)
	assert hasattr(server, "last") == False
	for client in clients:
		assert client.last["payload"] == br_from_server
		assert client.last["sender"] == server.id

	clients[0].send(br_with_server, target="**")
	sleep(2)
	assert server.last["payload"] == br_with_server
	assert server.last["sender"] == clients[0].id

	assert clients[0].last["payload"] == br_from_server
	assert clients[0].last["sender"] == server.id

	assert clients[1].last["payload"] == br_with_server
	assert clients[1].last["sender"] == clients[0].id

	assert clients[2].last["payload"] == br_with_server
	assert clients[2].last["sender"] == clients[0].id

	server.stop()


@mark.parametrize("server_params,client_params", parametrization)
def test_client_disconnect(server_params, client_params):
	server, clients = init_connections(server_params, client_params, count_of_clients=1)
	assert len(server._connections) == 1
	assert clients[0].enabled == True
	clients[0].disconnect()
	sleep(1)
	assert len(server._connections) == 0
	assert clients[0].enabled == False
	server.stop()

	server, clients = init_connections(server_params, client_params, count_of_clients=3)
	assert len(server._connections) == 3
	clients[0].disconnect()
	sleep(1)
	assert len(server._connections) == 2
	assert clients[0].enabled == False
	assert clients[1].id in server._connections.keys()
	assert clients[2].id in server._connections.keys()

	server.disconnect(clients[1].id)
	sleep(1)
	assert len(server._connections) == 1
	assert clients[1].enabled == False
	assert clients[2].id in server._connections.keys()
	server.stop()


def test_init_failed():
	try:
		remove("/tmp/weesocket-test.sock")
	except Exception:
		pass
	server_failed = False
	try:
		s = ServerWrapper(id="test-server", socket_file="")
	except OSError:
		s = None
		server_failed = True

	client_failed = False
	try:
		ClientWrapper(socket_file="")
	except OSError:
		client_failed = True

	assert server_failed == True
	assert client_failed == True
	if s:
		s.stop()


def test_listen_invalid_message():
	server, clients = init_connections(
		server_configs["file_socket"], client_configs["file_socket"],
		count_of_clients=1
	)
	invalid_message = b"10|0123456789"

	connection = server._connections[clients[0].id]
	connection._conn.sendall(invalid_message)
	sleep(2)
	assert hasattr(clients[0], "last") == False

	clients[0]._socket.sendall(invalid_message)
	sleep(2)
	assert hasattr(server, "last") == False

	server.stop()

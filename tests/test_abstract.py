from src.abstract import SocketAbstract


def test_abstract_initilaize():
	empty = SocketAbstract()
	assert empty.is_encrypted == False
	assert empty.secret == b""

	set = SocketAbstract(encrypt=True, secret=b"blabla")
	assert set.is_encrypted == True
	assert set.secret == b"blabla"

	raised = False
	try:
		set._trigger({}, "me")
	except NotImplementedError:
		raised = True

	assert raised == True


def test_abstract_check():
	abstract = SocketAbstract(encrypt=False)

	raw = b"5|asdf"
	not_complete, overload = abstract._check(raw)
	assert not_complete == b""
	assert overload == b"5|asdf"

	raw = b"5|asdfg"
	complete, no_overload = abstract._check(raw)
	assert complete == b"asdfg"
	assert no_overload == b""

	raw = b"5|asdfg7"
	complete, overload = abstract._check(raw)
	assert complete == b"asdfg"
	assert overload == b"7"

	raw = b"g|asdfg7"
	not_complete, no_overload = abstract._check(raw)
	assert not_complete == b""
	assert no_overload == b""


def test_abstract_sign():
	data = b"pivo"
	secret = b"secret"
	signature = b"441e6efeb77fe07ca043135d6d99255ae4bc3af1289c4cd09f223913eae98a15"

	assert signature == SocketAbstract._sign(data, secret)


def test_abstract_encryption():
	from rsa import newkeys
	keys = newkeys(2048)

	data = b"message to encrypt"

	encrypted = SocketAbstract._encrypt(data, keys[0])
	decrypted = SocketAbstract._decrypt(encrypted, keys[1])

	assert data == decrypted


def test_abstract_base():
	abstract = SocketAbstract(encrypt=False)
	to_dump = {
		"foo": "bar",
		"int": 12,
		"bool": True
	}

	dumped = abstract._dump(to_dump)
	loaded = abstract._load(b"|".join(dumped.split(b"|")[1:]))
	assert to_dump == loaded
	parts = dumped.split(b"|")
	assert int(parts[0]) == (len(parts[1]))

	secret = b"secret"
	abstract = SocketAbstract(encrypt=False, secret=secret)
	dumped = abstract._dump(to_dump)
	loaded = abstract._load(b"|".join(dumped.split(b"|")[1:]), secret)
	assert to_dump == loaded
	parts = dumped.split(b"|")
	assert int(parts[0]) == (len(parts[1]) + len(parts[2]) + 1)


def test_abstract_signature_failed():
	secret = b"secret"
	abstract = SocketAbstract(encrypt=False, secret=secret)
	to_dump = {
		"foo": "bar",
		"int": 12,
		"bool": True
	}

	dumped = abstract._dump(to_dump)
	is_invalid = False
	try:
		abstract._load(b"|".join(dumped.split(b"|")[1:]), b"secret2")
	except IOError:
		is_invalid = True
	assert is_invalid == True


def test_abstract_encrypted():
	from rsa import PublicKey
	abstract = SocketAbstract(
		encrypt=True,
	)
	to_dump = {
		"foo": "bar",
		"int": 12,
		"bool": True
	}

	dumped = abstract._dump(
		to_dump,
		remote_key=PublicKey.load_pkcs1(abstract._public_key)
	)
	loaded = abstract._load(b"|".join(dumped.split(b"|")[1:]))
	assert to_dump == loaded
	parts = dumped.split(b"|")
	assert int(parts[0]) == (len(parts[1]))

	secret = b"secret"
	abstract = SocketAbstract(
		encrypt=True, secret=secret,
	)
	dumped = abstract._dump(
		to_dump,
		remote_key=PublicKey.load_pkcs1(abstract._public_key)
	)
	loaded = abstract._load(b"|".join(dumped.split(b"|")[1:]), secret)
	assert to_dump == loaded
	parts = dumped.split(b"|")
	assert int(parts[0]) == (len(parts[1]) + len(parts[2]) + 1)


def test_abstract_log():
	abstract = SocketAbstract(encrypt=False)
	abstract._log("to log", level=30)

	import logging
	logger = logging.getLogger("test-logger")

	abstract = SocketAbstract(encrypt=False, logger=logger)
	abstract._log("to log", level=30)

from os import remove
from time import sleep
from rsa import newkeys
from src.server import Server
from src.client import Client


keys = newkeys(2048)
public_key_server = keys[0].save_pkcs1().decode()
private_key_server = keys[1].save_pkcs1().decode()

keys = newkeys(2048)
public_key_client = keys[0].save_pkcs1().decode()
private_key_client = keys[1].save_pkcs1().decode()


class ClientWrapper(Client):
	def _trigger(self, payload, sender):
		self.last = {
			"payload": payload,
			"sender": sender
		}


class ServerWrapper(Server):
	def _trigger(self, payload, sender):
		# print("in-server-trigger", payload, sender)
		self.last = {
			"payload": payload,
			"sender": sender
		}


def init_connections(server_params, client_params, count_of_clients=1, server_id="test-server"):
	try:
		remove("/tmp/weesocket-test.sock")
	except Exception:
		pass
	server = ServerWrapper(
		id=server_id,
		**server_params,
		public_key=public_key_server, private_key=private_key_server
	)
	clients = []
	for i in range(count_of_clients):
		clients.append(ClientWrapper(
			**client_params,
			public_key=public_key_client, private_key=private_key_client
		))
	counter = 0
	while True:
		if counter == 10:
			server.stop()
			try:
				remove("/tmp/weesocket-test.sock")
			except Exception:
				pass
			sleep(10)
			print("init connection retry...")
			return init_connections(
				server_params, client_params,
				count_of_clients=count_of_clients, server_id=server_id
			)
		counter += 1
		loaded = 0
		for client in clients:
			if client.id:
				loaded += 1
		if loaded == len(clients):
			break
		sleep(1)
	sleep(1)
	return server, clients


def try_until_true(condition_lambda, sleep_time=0.3, attempts=600):
	counter = 0
	while counter <= attempts:
		# print("in try", counter, condition_lambda())
		if condition_lambda():
			return
		counter += 1
		sleep(sleep_time)
